#pragma once
#include "GameStructure.h"
#include <string>
using namespace std;


class Character : public GameStructure
{
public:
	string Name;
	int GetHealth();
	void SetHealth(int health);
	void DisplayCharacterStats();

	
private:
	int Health;
};

