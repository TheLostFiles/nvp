#include "Character.h"
#include <iostream>


int Character::GetHealth()
{
	return Health;
}
void Character::SetHealth(int health)
{
	Health = health;
}
void Character::DisplayCharacterStats()
{
	cout << "This ninja is named" << Name <<" and has "<< GetHealth() <<"\n";
}