#include<iostream>
#include<string>

#include"Character.h"
#include"GameDriver.h"
#include"GameStructure.h"
#include"Ninjas.h"
#include"Pirates.h"


void main()
{
	//intro
	GameDriver game_driver;
	game_driver.DisplayInto();

	
	//ninjas
	Ninjas black_clothes_ninja;
	//name
	black_clothes_ninja.Name = "Bruce Lee";
	//health
	black_clothes_ninja.SetHealth(100);
	//health check
	black_clothes_ninja.DisplayCharacterStats();

	
}